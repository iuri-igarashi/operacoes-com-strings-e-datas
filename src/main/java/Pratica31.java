/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a1100629
 */
import java.util.Calendar;
import static java.util.Calendar.FEBRUARY;
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author a1100629
 */
/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica31 {

    //variaveis
    private static String meuNome = "Iuri Igarashi";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1992, FEBRUARY, 28);
    private static Date inicio = new Date();
    private static Date fim;

    public static void main(String[] args) {

        meuNome = meuNome.trim();
        Integer ultimoEspaco = 0;

        String s1 = "";
        String s2 = "";

        Integer numeroDias = 0;

        //sobrenome
        for (Integer i = meuNome.length() - 1; i >= 0; i--) {
            if (meuNome.charAt(i) == ' ') {
                s1 = meuNome.substring(i+1, i+2);
                s1 = s1.toUpperCase();
                
                s1 = s1 + (meuNome.substring(i + 2, meuNome.length())).toLowerCase();
                s1 = s1 + ",";
                ultimoEspaco = i;
                break;
            }
        }
        //nome
        for (Integer i = 0; i < ultimoEspaco; i++) {
            if (i == 0) {
                s2 = " " + meuNome.charAt(i) + ". ";
            } else if (meuNome.charAt(i) == ' ') {
                s2 = s2 + meuNome.charAt(i + 1) + ". ";
            }
            s2 = s2.toUpperCase();
        }

        //conta numero de dias ate hoje
        GregorianCalendar diaAtual = new GregorianCalendar();

        while ((dataNascimento.get(Calendar.YEAR) != diaAtual.get(Calendar.YEAR)) || (dataNascimento.get(Calendar.MONTH) != diaAtual.get(Calendar.MONTH)) || (dataNascimento.get(Calendar.DAY_OF_MONTH) != diaAtual.get(Calendar.DAY_OF_MONTH))) {
            dataNascimento.add(Calendar.DAY_OF_MONTH, 1);
            numeroDias++;
        }
        
        //imprime nome maiusculo
        System.out.println(meuNome.toUpperCase());
        //imprime nome
        System.out.println(s1 + s2);
        //imprime dias do nascimento ate hj
        System.out.println(numeroDias);
        //conta milisegundos
        fim = new Date();
        //imprime milisegundos
        System.out.println(fim.getTime() - inicio.getTime());

    }
}
